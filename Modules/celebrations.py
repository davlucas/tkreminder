"""
Display the birthdays and Events
"""

from tkinter import StringVar, Frame, Label
from datetime import date, datetime
class Celebration:
    """
    mother class: brthday ad events have same initialize
    """
    def __init__(self, frame, liste_info, place):
        print(liste_info)
        self.column = dict()
        self.row = dict()
        self.info = dict()
        self.stringvar = dict()
        self.stringvar["age"] = StringVar()
        self.stringvar["delta"] = StringVar()
        self.column["birthday"] = 0
        self.column["Event"] = 0
        self.row["birthday"] = 0
        self.row["Event"] = 0
        self.info["delta"] = 0
        self.info["age"] = 0
        self.frame_uni = Frame(frame, width=35)
        self.fill_frame(liste_info, place)

    def fill_frame(self, liste_info, place):
        return (self, liste_info, place)
    def delete(self):
        self.frame_uni.grid_forget()


class Birthday(Celebration):
    def fill_frame(self, liste_info, place):
        self.frame_uni.config(bg="white")
        Label(
            self.frame_uni,
            width=30,
            relief="groove",
            borderwidth=1,
            bg="white",
            fg="blue",
            font=("Helvetica", 12, "bold"),
            text=liste_info[0].upper(),
        ).grid(column=0, row=0, columnspan=4)
        Label(self.frame_uni, bg="white", fg="blue", text="will be").grid(
            column=0, row=1
        )
        Label(
            self.frame_uni,
            relief="groove",
            borderwidth=1,
            bg="#AAAAAA",
            fg="black",
            textvariable=self.stringvar["age"],
        ).grid(column=1, row=1)
        Label(self.frame_uni, bg="white", fg="blue", text="years old in").grid(
            column=2, row=1
        )
        Label(
            self.frame_uni,
            relief="groove",
            borderwidth=1,
            bg="#AAAAAA",
            fg="black",
            textvariable=self.stringvar["delta"],
        ).grid(column=3, row=1)
        self.frame_uni.grid(row=place[0], column=place[1])
        self.frame_uni.config(relief="ridge", borderwidth=3)
        self.fillstringvars(liste_info)

    def fillstringvars(self, liste):
        now = datetime.now()
        nowyear = int(now.year)
        nowmonth = int(now.month)
        nowday = int(now.day)
        table = liste[2].split("/")
        nowdate = date(nowyear, nowmonth, nowday)
        birthdaydate = date(int(table[0]), int(table[1]), int(table[2]))
        i = int(table[0])
        delta = (birthdaydate - nowdate).days
        while delta < 0:
            i += 1
            newd2 = date(i, int(table[1]), int(table[2]))
            delta = (newd2 - nowdate).days
        self.stringvar["delta"].set("{} days".format(delta))
        self.stringvar["age"].set(
            "{} ".format(int(((newd2 - birthdaydate).days) / 365))
        )
        return (delta, int(((newd2 - birthdaydate).days) / 365))


class Event(Celebration):
    def fill_frame(self, liste_info, place):
        self.frame_uni.config(bg="#000033")
        Label(
            self.frame_uni,
            width=30,
            relief="groove",
            borderwidth=3,
            bg="#333377",
            fg="#DDDDFF",
            text=liste_info[0],
        ).grid(column=0, row=0, columnspan=4)
        Label(
            self.frame_uni, bg="#000033", fg="#DDDDFF", text="will take place "
        ).grid(column=0, row=1)
        Label(self.frame_uni, bg="#000033", fg="#DDDDFF", text="in").grid(
            column=2, row=1
        )
        Label(
            self.frame_uni,
            relief="groove",
            borderwidth=3,
            bg="#333377",
            fg="#DDDDDD",
            textvariable=self.stringvar["delta"],
        ).grid(column=3, row=1)
        self.frame_uni.grid(row=place[0], column=place[1])
        self.frame_uni.config(relief="ridge", borderwidth=3)
        self.fillstringvars(liste_info)

    def fillstringvars(self, liste):
        now = datetime.now()
        nowyear = int(now.year)
        nowmonth = int(now.month)
        nowday = int(now.day)
        nowdate = date(nowyear, nowmonth, nowday)
        table = liste[2].split("/")
        eventdate = date(int(table[0]), int(table[1]), int(table[2]))
        self.stringvar["delta"].set(
            "{} days".format((eventdate - nowdate).days)
        )
